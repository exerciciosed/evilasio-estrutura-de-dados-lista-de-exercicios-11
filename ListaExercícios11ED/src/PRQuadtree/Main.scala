package PRQuadtree

object Main {
    def main(args: Array[String]){
     val region = new  Region(0,0, 100,100)
      
     val nostatico = new Node(region) 
      //cria arvore vazia
     var r = nostatico.createVoidTree(region)
     
     
     //insere nos na arvore
     r = nostatico.insert(r, new Node(region,52,45,1,"no1"))
     r = nostatico.insert(r, new Node(region,52,15,1,"no2"))
     r = nostatico.insert(r, new Node(region,1,4,1,"no3"))
     
     //Testando se insercoes foram bem sucedidas
     println(r.quadrante(2).quadrante(0).info)
     println(r.quadrante(2).quadrante(1).info)
     println(r.quadrante(1).info)
    
     //buscando posicao 52,45
     println(r.search(1,4).info)
     
     //remocao
     r.remove(52,15)
     
     //verificando remocao
     println(r.quadrante(2).info)
    }
}