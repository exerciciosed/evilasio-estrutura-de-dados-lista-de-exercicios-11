package PRQuadtree

import scala.collection.mutable.ArrayBuffer

class Region(var xo:Int, var yo:Int, var xf:Int, var yf:Int)

class Node(var region:Region, var x:Int, var y:Int, var cor:Int, var info:String,
           var quadrante:Array[Node]){
  
  def this(r:Region, x:Int=0, y:Int=0, cor:Int=0, info:String=""){
		this(r, x, y, cor, info, new Array[Node](4))
  }
  
  def createVoidTree(r:Region):Node = null
   
  //insercao
  def insert(root:Node, node:Node):Node={
    var pai, auxr :Node = null
    var q:Int = 0
    
    if(root == null){ //arvore vazia
      auxr = node
      auxr.cor = 1
    }
    else{
      if(root.cor != 2){
        auxr = new Node(root.region, (root.region.xo + root.region.xf)/2, (root.region.yo + root.region.yf)/2, 2)
        q = returnPosition(auxr, root.x, root.y) 
        auxr.quadrante(q) = root
        
        q = returnPosition(auxr, node.x, node.y)
        if(auxr.quadrante(q) == null) auxr.quadrante(q) = node 
        else{
          var r: Region = null
          q match{
            case 0 => r = new Region(root.region.xo, (root.region.yo + root.region.yf)/2, 
                                     (root.region.xo + root.region.xf)/2,root.region.yf) 
            case 1 => r = new Region(root.region.xo, root.region.yo, 
                                     (root.region.xo + root.region.xf)/2,(root.region.yo + root.region.yf)/2)
            case 2 => r = new Region((root.region.xo + root.region.xf)/2,root.region.yo, 
                                     root.region.xf,(root.region.yo + root.region.yf)/2)
            case 3 => r = new Region((root.region.xo + root.region.xf)/2,(root.region.yo + root.region.yf)/2,
                                      root.region.xf,root.region.yf)
          }
          auxr.quadrante(q).region = r       
          auxr.quadrante(q) = insert(auxr.quadrante(q), node)
        }
      }else{
        auxr = root
        q = returnPosition(auxr, node.x, node.y)
        if(auxr.quadrante(q) == null) auxr.quadrante(q) = node 
        else{
          var r: Region = null
          q match{
            case 0 => r = new Region(root.region.xo, (root.region.yo + root.region.yf)/2, 
                                     (root.region.xo + root.region.xf)/2,root.region.yf) 
            case 1 => r = new Region(root.region.xo, root.region.yo, 
                                     (root.region.xo + root.region.xf)/2,(root.region.yo + root.region.yf)/2)
            case 2 => r = new Region((root.region.xo + root.region.xf)/2,root.region.yo, 
                                     root.region.xf,(root.region.yo + root.region.yf)/2)
            case 3 => r = new Region((root.region.xo + root.region.xf)/2,(root.region.yo + root.region.yf)/2,
                                      root.region.xf,root.region.yf)
          }
          auxr.quadrante(q).region = r       
          auxr.quadrante(q) = insert(auxr.quadrante(q), node)
        }
      }
    }
    return auxr
  }
  
  //busca
  def search(x:Int, y:Int, root:Node=this):Node={
    if(root == null) return null
    else{
      if(root.x == x && root.y == y) return root
      else{
        var q = returnPosition(root, x, y)
        search(x, y, root.quadrante(q))
      }
    }
  }
  
  //Remocao
  def remove(x:Int, y:Int, root:Node=this, pai:Node=null):Node={
    if(root == null) return null
    else{
      if(root.x == x && root.y == y){
        if(pai == null) return null
        else{
          var q = returnPosition(pai, x, y)
          var brothers = returnBrothers(pai,q)
          if(brothers.length > 1){ pai.quadrante(q) = null; return pai}
          else return brothers(0)
        }
      }
      else{
        var q = returnPosition(root, x, y)
        var novoroot = remove(x, y, root.quadrante(q), root)
        if(pai!=null){
          var qpai = returnPosition(pai, root.x, root.y)
          pai.quadrante(qpai) = novoroot
        }
      }
     return pai 
    } 
  }
  
  //Retorna Irmaos
  def returnBrothers(root:Node, q:Int):ArrayBuffer[Node]={
    var irmaos = new ArrayBuffer[Node]
    for(i <- 0 until 4){
      if (root.quadrante(i) != null && i != q)
        irmaos += root.quadrante(i)
    }
    return irmaos
  }
  
  //Retorna Quadrante
  def returnPosition(root:Node, XX:Int, YY:Int):Int={
    if(root.x < XX){
      if(root.y < YY)
        return 3
      else
        return 2
    } else {
      if(root.y < YY)
        return 0
      else
        return 1
    }
  }
}